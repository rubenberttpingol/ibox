<section id="mainMenu">
	<nav class="navbar navbar-default navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="<?php echo base_url(); ?>">ideaBox</a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown"><a href="#" data-toggle="dropdown"><i class="fa fa-archive"></i> My iBox</a>
					<ul class="dropdown-menu">
						<li><a href="#"><i class="fa fa-pencil"></i> Jot Idea</a></li>
						<li><a href="#"><i class="fa fa-share"></i> Share Idea</a></li>
					</ul>
				</li>
				<li class="dropdown"><a href="#" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
					<ul class="dropdown-menu">
						<li class="dropdown-header">
							<div class="media">
								<div class="media-left">
									<a href="#">
										<?php
											echo "<img class='fb_profile' src="."https://graph.facebook.com/".$user_profile['data']['id']."/picture".">";
										?>
									</a>
								</div>
								<div class="media-body">
									<h5 class="media-heading"><?php echo $user_profile['data']['name']; ?></h5>
									<a href="#" class="btn btn-default btn-sm"><i class="fa fa-user"></i> My Profile</a>
								</div>
							</div>
						</li>
						<li class="nav-divider"></li>
						<li><a href="#">Account Settings</a></li>
						<li><a href="<?php echo $logout_url; ?>">Sign Out</a></li>
					</ul>
				</li>
			</ul>
			<form class="navbar-form navbar-right" role="search">
				<div class="form-group">
					<div class="input-group">
						<input type="search" class="form-control" name="q" placeholder="Search">
						<span class="input-group-btn">
							<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
						</span>
					</div>
				</div>
			</form>
		</div>
	</nav>
</section>
<!-- End of #mainMenu -->