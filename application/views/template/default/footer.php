
	<footer>
		<div class="container">
			<p>
				&copy; Copyright of <a href="<?php echo base_url(); ?>">IdeaBox</a>. All rights reserved 2016.
			</p>
		</div>
	</footer>
	
	<script src="<?php echo base_url('assets/twbs/js/jquery-1.12.3.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/twbs/js/bootstrap.min.js'); ?>"></script>
</body>
</html>