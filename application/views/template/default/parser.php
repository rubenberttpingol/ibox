<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Load Main Header
$this->load->view('template/default/header', $this->data);

/* Load Components */

// Load Main Navigation
if(isset($main_nav['display']) && $main_nav['display'] == true) {
	$this->load->view($main_nav['component_src'], $this->data);
}
// Load Sub-Header
if(isset($sub_header['display']) && $sub_header['display'] == true) {
	$this->load->view($sub_header['component_src'], $this->data);
}

/* End of Components */

// Load Main Content
$this->load->view($content, $this->data);

// Load Main Footer
$this->load->view('template/default/footer', $this->data);