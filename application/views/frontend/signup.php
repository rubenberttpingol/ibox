
	<section id="mainContent">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<?php $this->load->view('modules/mod_signup', $this->data); ?>
				</div>
			</div>
		</div>
	</section>
	<!-- End of #mainContent -->