
					<content class="loginForm">
						<div class="panel panel-default">
							<div class="panel-body">
								<h3 class="text-muted text-center">
									<b>iBox</b> Sign Up
								</h3>
								<div class="panel-body">
									<form role="form" action="" method="POST" id="loginForm">
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
												<input type="text" name="frmLoginUser" class="form-control" placeholder="Full Name">
											</div>
										</div>
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
												<input type="email" name="frmLoginEmail" class="form-control" placeholder="Email">
											</div>
										</div>
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-key"></i></span>
												<input type="password" name="frmLoginPassword" class="form-control" placeholder="Password">
											</div>
										</div>
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-lock"></i></span>
												<input type="password" name="frmLoginPassword" class="form-control" placeholder="Confirm Password">
											</div>
										</div>
										<div class="form-group">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="frmLoginRememberMe">
													<span class="small text-muted">
														Accept <a href="<?php echo base_url(); ?>">License Terms</a> and <a href="<?php echo base_url(); ?>">Agreements.</a>
													</span>
												</label>
											</div>
										</div>
										<hr>
										<div class="row">
											<div class="col-md-6">
												<a href="#" class="btn btn-primary btn-block">Sign Up with FB</a>
											</div>
											<div class="col-md-6">
												<a href="#" class="btn btn-danger btn-block">Sign Up with G+</a>
											</div>
										</div>
										<hr>
										<div class="form-group">
											<button type="submit" class="btn btn-primary pull-right">
												Sign Up
											</button>
											<span class="form-text small">
												<span class="text-muted">Has an account already?</span><br>
												<a href="<?php echo base_url('signin'); ?>">Sign In here!</a>
											</span>
										</div>
									</form>
									<!-- End of #loginForm -->
								</div>
								<!-- End of .panel-body -->
							</div>
						</div>
					</content>