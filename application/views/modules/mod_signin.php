
					<content class="loginForm">
						<div class="panel panel-default">
							<div class="panel-body">
								<h3 class="text-muted text-center">
									<b>iBox</b> Sign In
								</h3>
								<div class="panel-body">
									<form role="form" action="" method="POST" id="loginForm">


										<div class="form-group text-center text-muted">
											<span class="fa-stack fa-4x">
												<i class="fa fa-circle-o fa-stack-2x"></i>
												<i class="fa fa-user fa-stack-1x"></i>
											</span>
										</div>
										<hr>
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
												<input type="text" name="frmLoginUser" class="form-control" placeholder="Username or Email">
											</div>
										</div>
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-key"></i></span>
												<input type="password" name="frmLoginPassword" class="form-control" placeholder="Password">
											</div>
										</div>
										<div class="form-group">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="frmLoginRememberMe">
													<small>Remember Me</small>
												</label>
											</div>
										</div>
										<hr>
										<?php echo "<a href='$login_url' class='btn btn-primary'>Facebook Sign In</a>"; ?>
										<hr>
										<div class="form-group">
											<button type="submit" class="btn btn-primary pull-right">
												Sign In
											</button>
											<!-- <a href="<?php echo base_url('myaccount'); ?>" class="btn btn-primary pull-right">Sign In </a> -->
											<span class="form-text small">
												<span class="text-muted">Don't have an account yet?</span><br>
												<a href="<?php echo base_url('signup'); ?>">Register here!</a>
											</span>
										</div>
									</form>
									<!-- End of #loginForm -->
								</div>
								<!-- End of .panel-body -->
							</div>
						</div>
					</content>