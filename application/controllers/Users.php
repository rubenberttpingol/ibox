<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	
	public $data = array();
	public $user = "";

	public function __construct() {
		parent::__construct();
		$this->data['main_nav'] = array(
				'display'	=>	true,
				'component_src'	=>	'components/frontend_nav'
			);
		$this->data['sub_header'] = array(
				'display'		=>	true,
				'component_src'	=>	'components/frontend_sub_header'
			);
		$this->load->library('facebook');
		$this->load->helper('url');
	}

	public function signup() {

		$this->data['content'] = 'frontend/signup';

		$this->load->view('template/default/parser', $this->data);
	}

	public function signin() {

		if ($this->user) {

			redirect('myaccount');

		} else {

			// Store users facebook login url
			$this->data['login_url'] = $this->facebook->login_url();

			$this->data['content'] = 'frontend/signin';
			$this->load->view('template/default/parser', $this->data);
		}
	}

	public function signout() {
		$this->facebook->destroy_session();
		redirect(base_url());
	}

	public function authenticate() {

	}
}