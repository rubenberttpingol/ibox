<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {

	public $data = array();
	public $user = "";

	public function __construct() {
		parent::__construct();
		$this->data['main_nav'] = array(
				'display'		=>	true,
				'component_src'	=>	'components/frontend_nav'
			);
		$this->load->library('facebook');
		$this->load->helper('url');
		/*
		// Load facebook library and pass associative array which contains appId and secret key
		$this->load->library('facebook', array('appId' => '591710187659549', 'secret' => '88740fc5ba14e6ab0668084606762bdd'));
		// Get user's login information
		$this->user = $this->facebook->getUser();*/
	}

	public function index() {

		if ($this->facebook->logged_in())
		{
			$user = $this->facebook->user();

			if ($user['code'] === 200)
			{
				unset($user['data']['permissions']);
				$this->data['user'] = $user['data'];
			}

		}

		if ($this->facebook->logged_in()) {
			$this->account();
		} else {
			
			$this->data['login_url'] = $this->facebook->login_url();
			$this->data['content'] = 'landing_page';

			$this->load->view('template/default/parser', $this->data);
		}
	}

	public function account() {

		$this->data['user_profile'] = $this->facebook->user();

		// Get logout url of facebook
		$this->data['logout_url'] = $this->facebook->logout_url(array('next' => base_url('users/signout')));

		$this->data['main_nav']['component_src'] = 'components/user_main_nav';
		$this->data['content'] = 'frontend/myaccount';

		$this->load->view('template/default/parser', $this->data);

	}
}
