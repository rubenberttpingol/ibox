<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyAccount extends CI_Controller {
	
	public $data = array();
	public $user = "";

	public function __construct() {
		parent::__construct();
		$this->data['main_nav'] = array(
				'display'		=>	true,
				'component_src'	=>	'components/user_main_nav'
			);
		$this->data['sub_header'] = array(
				'display'		=>	true,
				'component_src'	=>	'components/user_sub_header'
			);
		// Load facebook library and pass associative array which contains appId and secret key
		$this->load->library('facebook', array('appId' => '591710187659549', 'secret' => '88740fc5ba14e6ab0668084606762bdd'));
		// Get user's login information
		$this->user = $this->facebook->getUser();
	}

	public function index() {

		if ($this->user) {

			$this->data['user_profile'] = $this->facebook->api('/me/');

			// Get logout url of facebook
			$this->data['logout_url'] = $this->facebook->getLogoutUrl(array('next' => base_url('users/signout')));

			$this->data['content'] = 'frontend/myaccount';

			$this->load->view('template/default/parser', $this->data);

		} else {
			redirect('users/signout');
		}
	}
}